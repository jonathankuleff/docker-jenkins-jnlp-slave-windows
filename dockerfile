# escape=`

# Get OpenJDK nanoserver container
FROM openjdk:8-nanoserver as openjdk

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]
ARG BASE_URL

# Get the Slave from the Jenkins server
RUN Invoke-WebRequest "$env:BASE_URL/jnlpJars/slave.jar" -OutFile 'slave.jar'


# Build off .NET Core nanoserver container
FROM microsoft/nanoserver:10.0.14393.1593

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

ENV JAVA_HOME c:\Program Files\openjdk

RUN setx /M PATH $($Env:PATH + ';' + $Env:JAVA_HOME)

# Copy java into the container
COPY --from=openjdk "C:\ojdkbuild" "$JAVA_HOME"

# Copy Jenkins JNLP Slave into container
COPY --from=openjdk "/slave.jar" "/slave.jar"

ENTRYPOINT ["java", "-jar", ".\slave.jar"]
